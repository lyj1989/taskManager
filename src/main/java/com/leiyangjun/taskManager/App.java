package com.leiyangjun.taskManager;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableTransactionManagement
//@EnableAutoConfiguration(exclude = { GsonAutoConfiguration.class })
@MapperScan("com.leiyangjun.mapper")
@EnableSwagger2
@EnableScheduling
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
    
    @Bean
	@Profile("linuxdev")
	public Docket createLinuxdevRestApi() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).enable(true).select()
				.apis(RequestHandlerSelectors.basePackage("com.leiyangjun.controller")) // 扫描文档注解的包路径
				.paths(PathSelectors.any())
				.build();
	}

	@Bean
	@Profile("localdev")
	public Docket createLocaldevRestApi() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).enable(true).select()
				.apis(RequestHandlerSelectors.basePackage("com.leiyangjun.controller")) // 扫描文档注解的包路径
				.paths(PathSelectors.any())
				.build();
	}

	@Bean
	@Profile("pro")
	public Docket createProRestApi() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).enable(true).select()
				.apis(RequestHandlerSelectors.basePackage("com.leiyangjun.controller")) // 扫描文档注解的包路径
				.paths(PathSelectors.any())
				.build();
	}

	/** 指定了页面显示的信息，标题、描述 */
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("后台api")
				.contact(new Contact("leiyj", "https://leiyj.com", "leiyj@leiyj.com"))
				.description("http://www.pdtools.net/tools/becron.jsp (开启表达式)")
				.version("v1.0").build();
	}
}
