package com.leiyangjun.taskManager.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.StringUtil;
import com.leiyangjun.taskManager.config.SysCode;
import com.leiyangjun.taskManager.model.BasicRsp;


/**
 * 登陆过滤器
 * @author 雷阳军
 *
 */
@Component
public class LoginHandle implements HandlerInterceptor {

	private final Logger log = LoggerFactory.getLogger(LoginHandle.class);
	
//	@Autowired
//	private TokensMapper tokensMapper;

	public LoginHandle() {
		
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object object,
			Exception exception) throws Exception {
		log.info("afterCompletion:" + request.getServletPath());
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object object,
			ModelAndView modelAndView) throws Exception {
		log.info("postHandle:" + request.getServletPath());
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception {
		log.info("postHandle:" + request.getServletPath());
		String token=request.getHeader("token");
		Boolean tokenResult=true;//tokensMapper.getValidToken(token)==null?false:true;
		
		String origin = request.getHeader("Origin");
		response.setHeader("Access-Control-Allow-Origin", StringUtil.isEmpty(origin) ? "*" : origin);
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT, PATCH");
        response.setHeader("Access-Control-Max-Age", "0");
        response.setHeader("Access-Control-Allow-Headers", "Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With,userId,token");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("XDomainRequestAllowed","1");
		if(!tokenResult) {
			SysCode sysCode=SysCode.SESSION_TIMEOUT;
			response.setCharacterEncoding("UTF-8");
			response.setHeader("Content-type", "text/json;charset=UTF-8");
			response.getWriter().println(JSONObject.toJSONString(new BasicRsp(sysCode.getCode(),sysCode.getMsg(),sysCode.getSuccessFlag(),null)));
		}
		return tokenResult;
	}
}

