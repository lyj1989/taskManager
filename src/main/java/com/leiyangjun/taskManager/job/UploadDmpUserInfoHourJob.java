package com.leiyangjun.taskManager.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * 用户信息上报,一小时一次
 * @author 雷阳军
 *
 */
public class UploadDmpUserInfoHourJob extends QuartzJobBean{

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("UploadDmpUserInfoHourJob job!");
	}

}
