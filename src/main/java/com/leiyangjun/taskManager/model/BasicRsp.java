package com.leiyangjun.taskManager.model;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

/**
 * api 返回数据模型
 * @author 雷阳军
 *
 */
public class BasicRsp implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4452212012145475442L;

	@ApiModelProperty(value = "接口状态码,1:成功,-1:失败", required = true, dataType = "Integer")
	private Integer code;
	
	@ApiModelProperty(value = "错误信息描述", required = true, dataType = "String")
	private String msg;
	
	@ApiModelProperty(value = "错误标记", required = true, dataType = "Boolean")
	private Boolean successFlag;
	
	@ApiModelProperty(value = "返回数据", required = true, dataType = "String")
	private Object data;
	
	public BasicRsp() {
		
	}

	public BasicRsp(Integer code, String msg, Boolean successFlag, Object data) {
		super();
		this.code = code;
		this.msg = msg;
		this.successFlag = successFlag;
		this.data = data;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Boolean getSuccessFlag() {
		return successFlag;
	}

	public void setSuccessFlag(Boolean successFlag) {
		this.successFlag = successFlag;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "BasicRsp [code=" + code + ", msg=" + msg + ", successFlag=" + successFlag + ", data=" + data + "]";
	}
	
}
