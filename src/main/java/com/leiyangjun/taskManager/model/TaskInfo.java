package com.leiyangjun.taskManager.model;

import java.util.Date;

/**
 * 定时任务数据模型
 * @author admin
 *
 */
public class TaskInfo {

	private String name;
	
	private String group;
	
	private String description;
	
	private Date nextTime;
	
	private Date previousTime;
	
	private String jobStatus;
	
	private String cronExpression;
	
	private String classJob;
	
	public TaskInfo() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getNextTime() {
		return nextTime;
	}

	public void setNextTime(Date nextTime) {
		this.nextTime = nextTime;
	}

	public Date getPreviousTime() {
		return previousTime;
	}

	public void setPreviousTime(Date previousTime) {
		this.previousTime = previousTime;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}
	
	public String getClassJob() {
		return classJob;
	}

	public void setClassJob(String classJob) {
		this.classJob = classJob;
	}

	@Override
	public String toString() {
		return "TaskInfo [name=" + name + ", group=" + group + ", description=" + description + ", nextTime=" + nextTime
				+ ", previousTime=" + previousTime + ", jobStatus=" + jobStatus + ", cronExpression=" + cronExpression
				+ "]";
	}
	
	
}
