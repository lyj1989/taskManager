package com.leiyangjun.taskManager.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.leiyangjun.taskManager.config.SysCode;
import com.leiyangjun.taskManager.model.BasicRsp;
import com.leiyangjun.taskManager.model.TaskInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("task")
@Api(value = "task", tags = { "1.0,定时任务管理api" })
public class TaskController {

	private static Logger log = LoggerFactory.getLogger(TaskController.class);

	@Autowired
	private Scheduler scheduler;

	@Autowired
	private SchedulerFactoryBean schedulerFactoryBean;

	@ApiOperation(value = "添加/修改任务,参数:{name:jobname,group:groupname,classJob:com.leiyangjun.job.AbcJob(实际任务执行者Class),description:任务描述,cronExpression:任务执行表达式}")
	@RequestMapping(value = "/addOrUpdate", method = RequestMethod.POST)
	public BasicRsp addOrUpdate(@RequestBody TaskInfo params, HttpServletRequest req) {
		SysCode dspCode = null;
		try {
			JobKey jobKey = new JobKey(params.getName(), params.getGroup());
			if (scheduler.checkExists(jobKey)) {
				TriggerKey triggerKey = TriggerKey.triggerKey(params.getName(), params.getGroup());
				scheduler.pauseTrigger(triggerKey);// 停止触发器
	            scheduler.unscheduleJob(triggerKey);// 移除触发器
	            scheduler.deleteJob(jobKey);
			}
			Class t = Class.forName(params.getClassJob());
			JobDetail job = JobBuilder.newJob(t).withIdentity(params.getName(), params.getGroup())
					.withDescription(params.getDescription()).build();
			Trigger trigger = TriggerBuilder.newTrigger()
					.withSchedule(CronScheduleBuilder.cronSchedule(params.getCronExpression())).forJob(job).build();
			scheduler.scheduleJob(job, trigger);
			if (!scheduler.isShutdown()) {
				scheduler.start();
			}
			dspCode = SysCode.SUCCESS;
		} catch (Exception e) {
			log.error("", e);
			dspCode = SysCode.SYS_ERROR;
		}
		return new BasicRsp(dspCode.getCode(), dspCode.getMsg(), dspCode.getSuccessFlag(), null);
	}

	@ApiOperation(value = "删除任务,参数:{name:jobname,group:groupname}")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public BasicRsp delete(@RequestBody TaskInfo params, HttpServletRequest req) {
		SysCode dspCode = null;
		try {
			JobKey jobKey = new JobKey(params.getName(), params.getGroup());
			if (scheduler.checkExists(jobKey)) {
				TriggerKey triggerKey = TriggerKey.triggerKey(params.getName(), params.getGroup());
				scheduler.pauseTrigger(triggerKey);// 停止触发器
	            scheduler.unscheduleJob(triggerKey);// 移除触发器
	            scheduler.deleteJob(jobKey);
			}
			dspCode = SysCode.SUCCESS;
		} catch (Exception e) {
			log.error("", e);
			dspCode = SysCode.SYS_ERROR;
		}
		return new BasicRsp(dspCode.getCode(), dspCode.getMsg(), dspCode.getSuccessFlag(), null);
	}

	@ApiOperation(value = "暂停任务,参数:{name:jobname,group:groupname}")
	@RequestMapping(value = "/pause", method = RequestMethod.POST)
	public BasicRsp pause(@RequestBody TaskInfo params, HttpServletRequest req) {
		SysCode dspCode = null;
		try {
			JobKey jobKey = new JobKey(params.getName(), params.getGroup());
			if (scheduler.checkExists(jobKey)) {
				scheduler.pauseJob(jobKey);// 停止触发器
			}
			dspCode = SysCode.SUCCESS;
		} catch (Exception e) {
			log.error("", e);
			dspCode = SysCode.SYS_ERROR;
		}
		return new BasicRsp(dspCode.getCode(), dspCode.getMsg(), dspCode.getSuccessFlag(), null);
	}
	
	@ApiOperation(value = "恢复任务,参数:{name:jobname,group:groupname}")
	@RequestMapping(value = "/resume", method = RequestMethod.POST)
	public BasicRsp resume(@RequestBody TaskInfo params, HttpServletRequest req) {
		SysCode dspCode = null;
		try {
			JobKey jobKey = new JobKey(params.getName(), params.getGroup());
			if (scheduler.checkExists(jobKey)) {
				scheduler.resumeJob(jobKey);// 停止触发器
			}
			dspCode = SysCode.SUCCESS;
		} catch (Exception e) {
			log.error("", e);
			dspCode = SysCode.SYS_ERROR;
		}
		return new BasicRsp(dspCode.getCode(), dspCode.getMsg(), dspCode.getSuccessFlag(), null);
	}

	@ApiOperation(value = "任务列表")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public BasicRsp list(HttpServletRequest req) {
		List<TaskInfo> jobList = new ArrayList<TaskInfo>();
		Scheduler scheduler = schedulerFactoryBean.getScheduler();
		GroupMatcher<JobKey> matcher = GroupMatcher.anyJobGroup();
		try {
			Set<JobKey> jobKeys = scheduler.getJobKeys(matcher);
			for (JobKey jobKey : jobKeys) {
				List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobKey);
				JobDetail jobDetail = scheduler.getJobDetail(jobKey);
				for (Trigger trigger : triggers) {
					final TaskInfo info = new TaskInfo();
					info.setName(jobKey.getName());
					info.setGroup(jobKey.getGroup());
					info.setDescription(jobDetail.getDescription()==null?"":jobDetail.getDescription());
					info.setNextTime(trigger.getNextFireTime()); // 下次触发时间
					// trigger.getFinalFireTime();//最后一次执行时间
					info.setPreviousTime(trigger.getPreviousFireTime());// 上次触发时间
					// trigger.getStartTime();//开始时间
					// trigger.getEndTime();//结束时间
					// 触发器当前状态
					Trigger.TriggerState triggerState = scheduler.getTriggerState(trigger.getKey());
					info.setJobStatus(triggerState.name());

					info.setClassJob(jobDetail.getJobClass().getName());
					if (trigger instanceof CronTrigger) {
						CronTrigger cronTrigger = (CronTrigger) trigger;
						String cronExpression = cronTrigger.getCronExpression();
						info.setCronExpression(cronExpression);
					}
					jobList.add(info);
				}
			}
		} catch (SchedulerException e) {
			log.error("", e);
		}
		SysCode dspCode = SysCode.SUCCESS;
		return new BasicRsp(dspCode.getCode(), dspCode.getMsg(), dspCode.getSuccessFlag(), jobList);
	}
}
