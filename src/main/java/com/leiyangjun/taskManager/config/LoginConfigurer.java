package com.leiyangjun.taskManager.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.leiyangjun.taskManager.filter.LoginHandle;

/**
 * token 登陆拦截器,加载器
 * @author 雷阳军
 *
 */
@Configuration
public class LoginConfigurer implements WebMvcConfigurer  {

	@Bean
	public LoginHandle myInterceptor() {
		return new LoginHandle();
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(myInterceptor()).addPathPatterns("/account/**","/brand/**","/channel/**","/band/**").
		excludePathPatterns(
				"/account/login",
				"/test/test",
				"/band/downloadFile",
				"/account/sendSmsCode"
		);
	}

}

