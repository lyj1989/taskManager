package com.leiyangjun.taskManager.config;

public enum SysCode {

	UNIQUE_FAIL(false,-1,"重复值错误，请重试！"),
	ERROR_SMSCODE_FAIL(false,-2,"短信发送失败,请稍后再试!"),
	ERROR_LOGIN_FAIL(false,-3,"手机号码或验证码错误"),
	SYS_ERROR(false,-4,"系统错误,请稍后再试"),
	MOBILEEXIST_ERROR(false,-5,"手机号码已存在"),
	ACCOUNTIDEXIST_ERROR(false,-6,"用户ID已存在"),
	FILE_ERROR(false,-7,"文件不合法"),
	ERROR_MOBILE_FAIL(false,-8,"未注册用户!"),
	NAMEEXIST_ERROR(false,-9,"该名称已经存在!"),
	REQUIRED_ERROR(false,0,"必填参数校验不通过(不符合格式要求或没有填写)"),
	SUCCESS(true,1,"成功"),
	SESSION_TIMEOUT(false,-100,"未登陆或登陆超时");
	
	private Integer code;
	
	private String msg;
	
	private Boolean successFlag;
	
	SysCode(Boolean successFlag,Integer code,String msg) {
		this.successFlag=successFlag;
		this.code=code;
		this.msg=msg;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Boolean getSuccessFlag() {
		return successFlag;
	}

	public void setSuccessFlag(Boolean successFlag) {
		this.successFlag = successFlag;
	}

}
